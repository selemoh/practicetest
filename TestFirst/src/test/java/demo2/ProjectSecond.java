package demo2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class ProjectSecond {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@Test(priority=1)
	public void loginTesting1() throws InterruptedException{

	System.setProperty("webdriver.gecko.driver", "D:\\testProject\\TestFirst\\libs\\geckodriver.exe");	
		
		driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		driver.manage().deleteAllCookies();
		
		wait = new WebDriverWait(driver, 30);
		
		driver.get("https://waypals.com/wpl");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='text']"))).sendKeys("mohit@waypals");
		
		Thread.sleep(5000);
		
		driver.quit();	
	}
	
	
	@Test(priority=2)
	public void loginTesting2() throws InterruptedException{

	System.setProperty("webdriver.gecko.driver", "D:\\testProject\\TestFirst\\libs\\geckodriver.exe");	
		
		driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		driver.manage().deleteAllCookies();
		
		wait = new WebDriverWait(driver, 30);
		
		driver.get("https://waypals.com/wpl");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password']"))).sendKeys("accountPassword");
		
		Thread.sleep(5000);
		
		driver.quit();	
	}
}